import Ember from 'ember';

export default Ember.Component.extend({
  value: '',
  edit: false,

  actions: {
    toggleDone() {
      this.sendAction('toggleDone', this.todo.id);
    },

    editTodo() {
      // this.set('edit', true);
      this.toggleProperty('edit');
      this.set('value', this.todo.title);
    },

    updateTodo() {
      this.sendAction('updateTodo', this.todo.id, this.value);
      // this.set('edit', false);
      this.toggleProperty('edit');
    },

    deleteTodo() {
      this.sendAction('deleteTodo', this.todo.id);
    }
  }
});
