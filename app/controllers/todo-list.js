import Ember from 'ember';

export default Ember.Controller.extend({
  storage: Ember.inject.service('local-storage'),
  value: '',
  idCount: 0,
  todos: [],

  init() {
    this.set('idCount', this.get('storage').getItem('emberTodosId') || 0);
    this.set('todos', this.get('storage').getItem('emberTodos') || []);
  },

  completed: Ember.computed.filterBy('todos', 'done', true),
  incompleted: Ember.computed.filterBy('todos', 'done', false),

  addTodo: function(title) {
    if (this.value) {
      const todo = {
        id: this.idCount,
        title: title,
        done: false
      };
      this.todos.pushObject(todo);
      // this.idCount++;
      this.incrementProperty('idCount');
      this.set('value', '');

      this.get('storage').setItem('emberTodosId', this.get('idCount'));
      this.get('storage').setItem('emberTodos', this.get('todos'));
    }
  },

  actions: {
    addTodoItem() {
      this.addTodo(this.value);
    },

    deleteTodo(id) {
      const newTodos = this.todos.filter(todo => todo.id !== id);
      this.set('todos', newTodos);

      this.get('storage').setItem('emberTodos', this.get('todos'));
    },

    updateTodo(id, title) {
      const index = this.todos.findIndex(t => t.id === id);
      Ember.set(this.todos[index], 'title', title);

      this.get('storage').setItem('emberTodos', this.get('todos'));
    },

    toggleDone(id) {
      const index = this.todos.findIndex(t => t.id === id);
      Ember.set(this.todos[index], 'done', !this.todos[index].done);

      this.get('storage').setItem('emberTodos', this.get('todos'));
    }
  }
});
