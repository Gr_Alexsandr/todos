import Ember from 'ember';

export default Ember.Service.extend({
  store: Ember.inject.service(),

  init() {
    this._super(...arguments);

    this.seedTeam();
  },

  seedTeam() {
    let teamName = ['Team 1','Team 2','Team 3','Team 4'];

    for(let i = 0; i < teamName.length; i++) {
      // console.log({id: i, title: teamName[i], done: false});
      this.get('store').createRecord('todo-item', {id: i, title: teamName[i], done: false});
    }
  },

  getItem(name) {
    return JSON.parse(localStorage.getItem(name));
  },

  setItem(name, item) {
    localStorage.setItem(name, JSON.stringify(item));
  }
});
