import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel() {
    this.replaceWith('');
  },

  model() {
    return Ember.$.get('https://jsonplaceholder.typicode.com/todos').then(data => {
      return data;
    });
  }
});
