import Ember from 'ember';

export function addOne(params) {
  return +params + 1;
}

export default Ember.Helper.helper(addOne);
